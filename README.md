# Esterel Executable Semantics in Prolog

This repository contains a draft of an executable semantics of Esterel
written in Prolog. It's based on the original Structural Operational Semantics
of Esterel. For now, only the basic behavioral semantics is supported and not
the constructive one. Please note that this is probably the worst way to do
an interpreter. However, this allows early testing of a semantics.

## Usage


```prolog
?- consult(src/run).
?- step(present(a, emit(b), nothing), [a], O, K, R).
O = [b]
K = 0,
R = nothing.
?- step(par(emit(c),seq(emit(d),pause)), [], O, K, R).
O = [c, d],
K = 1,
R = par(nothing, nothing).
```

## Testing

```prolog
?- consult(tests/test_semantics).
?- run_tests.
```

## Requirements
- SWI-Prolog

## References

1. [A Structural Approach to Operational Semantics, G. D. Plotkin](http://homepages.inf.ed.ac.uk/gdp/publications/sos_jlap.pdf)
2. [The Constructive Semantics of Pure Esterel, Chap. 6, G. Berry](http://www-sop.inria.fr/members/Gerard.Berry/Papers/EsterelConstructiveBook.pdf)
