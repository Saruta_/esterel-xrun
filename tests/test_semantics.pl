:- consult('../src/run').

:- begin_tests(semantics).

% 1. basic rules
test(nothing) :- 
    step(nothing,_,[],0,nothing).
test(pause) :- 
    step(pause,_,[],1,nothing).
test(emit) :- 
    step(emit(sig),_,[sig],0,nothing).
test(exit) :-
    step(exit(2),_,[],2,nothing).

% 2. signal testing and suspension
test(present_then) :-
    step(present(sig,emit(sig_then),emit(sig_else)),
	 [sig],[sig_then],0,nothing).
test(present_else) :-
    step(present(sig,emit(sig_then),emit(sig_else)),
	 [],[sig_else],0,nothing).
% TODO suspend

% 3. sequence
test(seq1) :-
    step(seq(emit(a),emit(b)),
	 [],[a,b],0,nothing).
test(seq2) :-
    step(seq(pause,emit(a)),
	 [],[],1,seq(nothing,emit(a))).

% 4. loop
test(loop) :-
    step(loop(pause),[],[],1,seq(nothing,loop(pause))).

% 5. parallel composition and trap
test(par) :-
    step(par(emit(a),pause),
	 [],[a],1,par(nothing,nothing)).
test(trap1) :-
    step(trap(2,exit(2)),
	 [],[],0,nothing).
test(trap2) :-
    step(trap(2,pause),
	 [],[],1,trap(2,nothing)).

% 6. local signals
test(signalin) :-
    step(signalin(sig,
		  par(emit(sig),
		      present(sig,emit(out),nothing)
		     )),
	 [],[sig,out],0,nothing).

:- end_tests(semantics).
