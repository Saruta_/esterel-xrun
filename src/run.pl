%% global steps relations

:- use_module(library(pprint)).

:- consult('syntax').
:- consult('semantics').

% definition of estep/5 rule: step with extended syntax

estep(P1,E1,E2,K,P3) :-
    extend(P1,P2), step(P2,E1,E2,K,P3).

% definition of istep/1 rule: step separated by keyboard input

istep(S1,N) :-
    format('################ STEP ~w ###############\n', [N]),
    read(I),
    estep(S1,I,O,K1,S2),
    format('* input: ~w\n', [I]),
    format('* output: ~w\n', [O]),
    format('* return code: ~w\n', [K1]),
    format('* reduction: \n'),
    print_term(S2, [ident_arguments(true)]),
    format('\n'),
    N2 is N + 1,
    ((K1 = 0, !);
     (K1 > 0,
      istep(S2, N2))).

% definition of lstep/1 rule: step on each element of L

lstep(S1,[HI|TI],[HO|TO]) :-
    estep(S1,HI,HO,K,S2),
    ((K = 0, !);
     (K > 0,
      lstep(S2,TI,TO))).

lstep(S1,[H],[T]) :-
    estep(S1,H,T,_,_).
