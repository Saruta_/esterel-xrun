%% global utils

% maplist_builder/3 build a list with n times E element

maplist_builder(E, N, L) :-
    length(L, N),
    maplist(=(E), L).
