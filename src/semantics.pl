%% executable semantics of Esterel

:- consult('syntax').

% trap handling
trap_exit(0,0).
trap_exit(1,1).
trap_exit(2,0).
trap_exit(K1,K2) :- K1 > 2, K1 - 1 = K2.

% cleaning of dead terms
delta(P,K,nothing) :- term(P), K \= 1.
delta(P,K,P) :- term(P), K = 1.

% definition of step/5 rule:

% 1. basic rules
step(nothing,_,[],0,nothing).
step(pause,_,[],1,nothing).
step(emit(S),_,[S],0,nothing).
step(exit(KT),_,[],KT,nothing).

% 2. signal testing and suspension
step(present(S,P1,_),E1,E2,K,P2) :-
    member(S,E1),
    step(P1,E1,E2,K,P2).
step(present(S,_,Q1),E1,E2,K,Q2) :-
    not(member(S,E1)),
    step(Q1,E1,E2,K,Q2).
step(suspend(P1,S),E1,E2,K,P4) :-
    step(P1,E1,E2,K,P2),
    extend(seq(awaitnotimm(S),suspend(P2,S)),P3),
    delta(P3,K,P4).

% 3. sequence
step(seq(P1,Q),E1,E2,K,P3) :-
    step(P1,E1,E2,K,P2),
    K \= 0,
    delta(seq(P2,Q),K,P3).
step(seq(P1,Q1),E1,E4,K2,Q2) :-
    step(P1,E1,E2,K1,_),
    K1 = 0,
    step(Q1,E1,E3,K2,Q2),
    union(E2,E3,E4).

% 4. loop
step(loop(P1),E1,E2,K,R) :-
    step(P1,E1,E2,K,P2),
    K \= 0,
    delta(seq(P2,loop(P1)),K,R).

% 5. parallel composition and trap
step(par(P1,Q1),E1,E4,M,R) :-
    step(P1,E1,E2,K,P2),
    step(Q1,E1,E3,L,Q2),
    M is max(K,L),
    delta(par(P2,Q2),M,R),
    union(E2,E3,E4).
step(trap(KT,P1),E1,E2,K2,R) :-
    step(P1,E1,E2,K1,P2),
    trap_exit(K1,K2),
    delta(trap(KT,P2),K2,R).

% 6. local signals
step(signalin(S,P1),E1,E3,K,R) :-
    union(E1,[S],E2),
    step(P1,E2,E3,K,P2),
    member(S,E3),
    %subtract(E3,[S],E4),
    delta(signalin(S,P2),K,R).
step(signalin(S,P1),E1,E3,K,R) :-
    subtract(E1,[S],E2),
    step(P1,E2,E3,K,P2),
    not(member(S,E3)),
    delta(signalin(S,P2),K,R).
