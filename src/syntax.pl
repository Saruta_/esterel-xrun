%% general syntax of pure Esterel

:- consult('utils').

% pure signal is represented as Lisp atoms
signal(S) :- atom(S).

% pure abstract syntax
term(nothing).
term(pause).
term(emit(S)) :- signal(S).
term(present(S,P,Q)) :- signal(S), term(P), term(Q).
term(suspend(P,S)) :- term(P), signal(S).
term(seq(P,Q)) :- term(P), term(Q).
term(loop(P)) :- term(P).
term(par(P,Q)) :- term(P), term(Q).
term(trap(T,P)) :- integer(T), T > 1, term(P).
term(exit(T)) :- integer(T).
term(signalin(S,P)) :- signal(S), term(P).

% extended syntax
extend([H|T],seq(H2,T2)) :- extend(H,H2), extend(T,T2).
extend([H],H2) :- extend(H, H2).
extend(nothing,nothing).
extend(pause,pause).
extend(emit(S),emit(S)).
extend(present(S,P,Q),present(S,P2,Q2)) :-
    extend(P,P2),
    extend(Q,Q2).
extend(suspend(P,S),suspend(P2,S)) :-
    extend(P,P2).
extend(seq(P,Q),seq(P2,Q2)) :-
    extend(P,P2),
    extend(Q,Q2).
extend(loop(P),loop(P2)) :-
    extend(P,P2).
extend(par(P,Q),par(P2,Q2)) :-
    extend(P,P2),
    extend(Q,Q2).
extend(trap(T,P),trap(T,P2)) :-
    extend(P,P2).
extend(exit(T),exit(T)).
extend(signalin(S,P),signalin(S,P2)) :-
    extend(P,P2).
extend(halt,loop(pause)).
extend(sustain(S),
       loop(
	   seq(emit(S),pause)
       )).
extend(present(S,P),
       present(S,P2,nothing)) :-
    extend(P,P2).
extend(await(S),
       trap(2,
	    loop(
		seq(pause,present(S,exit(2),nothing))
	    ))).
extend(awaitnot(S),
       trap(2,
	    loop(
		seq(pause,present(S,nothing,exit(2)))
	    ))).
extend(awaitimm(S),
       trap(2,
	    loop(
		seq(present(S,exit(2),nothing),pause)
	    ))).
extend(awaitnotimm(S),
       trap(2,
	    loop(
		seq(present(S,nothing,exit(2)),pause)
	    ))).
extend(suspendimm(P,S),
       suspend(
	   seq(present(S, pause,nothing),P2),
	   S)) :-
    extend(P,P2).
extend(abort(P,S),
       trap(2,
	    par(
		seq(suspend(P2,S),exit(2)),
		loop(seq(pause,present(S,exit(2),nothing)))
	    ))) :-
    extend(P,P2).
extend(abortweak(P,S),
       trap(2,
	    par(
		seq(P2,exit(2)),
		loop(seq(pause,present(S,exit(2),nothing)))
	    ))) :-
    extend(P,P2).
% TODO abortimm, abortweakimm
extend(loopeach(P,S),loop(P2)) :-
    extend(abort(seq(P,loop(pause)),S),P2).
extend(every(S,P),seq(P2,P3)) :-
    extend(await(S),P2),
    extend(loopeach(P,S),P3).
extend(everyimm(S,P),seq(P2,P3)) :-
    extend(awaitimm(S),P2),
    extend(loopeach(P,S),P3).

% extension with n element
extend(awaitn(S,N), P) :-
    maplist_builder(await(S),N,L),
    extend(L,P).
